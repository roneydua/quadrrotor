#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
caso com realimentacao dos estados de posicao e velocidade translacionais
"""
import numpy as np
from numpy import sin, cos, tan

Inercia = [1.03, 16.83e-3, 16.83e-3, 28.34e-3]  # [m Ix Iy Iz]
braco = 0.26
g = np.matrix([0, 0, 9.82]).T
kf = 1.4351e-5
km = 2.4086e-7
OMEGA = np.matrix([([kf, kf, kf, kf]),
                   [0.0, -braco*kf, .0, braco*kf],
                   [-braco*kf, .0, braco*kf, .0],
                   [-km, km, -km, km]])**-1
# constante proporcionais
# Kv = [10., 10., 10.]
# altEquador = 6.371e6  # metros
# GM = 3.986e14  # m^3 / s^2


def planta(X):
    sf = sin(X[6, 0])
    cf = cos(X[6, 0])
    st = sin(X[8, 0])
    ct = cos(X[8, 0])
    tt = tan(X[8, 0])
#    cp = cos(X[10, 0])
#    sp = sin(X[10, 0])
    Ix = (Inercia[2]-Inercia[3])/Inercia[1]
    Iy = (Inercia[3]-Inercia[1])/Inercia[2]
    Iz = (Inercia[1]-Inercia[2])/Inercia[3]
    Ap = np.matrix([[0., 1., 0., 0., 0., 0., 0, 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 1., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 1., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 1., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 1., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 1.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.]])
    a1 = (1-Iy*sf**2+Iz*cf**2)*tt
    a2 = st*sf*cf*(Iy+Iz)
    a3 = 1/ct+ct*(cf**2-sf**2)*Ix +st*tt*(Iy*sf**2-Iz*cf**2)
    a4 = -Ix*sf*cf
    a5 = sf*cf*(Ix*ct**2-(Iy+Iz)*st**2)
    a6 = -sf*cf*(Iz+Iy)
    a7 = ct*(-1+Iy*cf**2-Iz*sf**2)
    a8 = st*ct*(Iz*sf**2- Iy*cf**2)
    a9 = 1/ct*(1-Iy*sf**2+Iz*cf**2)
    a10 = tt*(1+Iy*sf**2-Iz*cf**2)


#    Coefientes de momento giroscopios nao programados
    Ap[7, 7] = a1*X[9, 0]+a2*X[11, 0]
    Ap[7, 9] = a3*X[11, 0] + a4*X[9, 0]
    Ap[7, 11] = a5*X[11, 0]
    Ap[9, 7] = a6*X[9, 0]+a7*X[11, 0]
    Ap[9, 9] = a2*X[11, 0]
    Ap[9, 11] = a8*X[11, 0]
    Ap[11, 7] = a9*X[9, 0]-a6*X[11, 0]
    Ap[11, 9] = a10*X[11, 0]
    Ap[11, 11] = -a2*X[11, 0]
    Bp = np.matrix(np.zeros(shape=(12, 6)))
    b1 = 1/Inercia[1]
    b2 = tt*sf/Inercia[2]
    b3 = tt*cf/Inercia[3]
    b4 = cf/Inercia[2]
    b5 = -sf*Inercia[3]
    b6 = sf/ct/Inercia[2]
    b7 = cf/ct/Inercia[3]
    Bp[1, 0] = 1.0
    Bp[3, 1] = 1.0
    Bp[5, 2] = 1.0
    Bp[7, 3] = b1
    Bp[7, 4] = b2
    Bp[7, 5] = b3
    Bp[9, 4] = b4
    Bp[9, 5] = b5
    Bp[11, 4] = b6
    Bp[11, 5] = b7
    return Ap, Bp


def calcmatrizesCD():
    Cp = np.matrix(np.eye(12))
    Dp = np.matrix(np.zeros((12, 6)))
    return Cp, Dp


def atualizaRota(r, dt, UU):
    rn = 1.0*r
    U = UU + g
#   calcula theta
    rn[0, 0] = np.arctan((U[0, 0]*cos(r[4, 0])+U[1, 0]*sin(r[4, 0]))/( U[2, 0]))
#   computa fi
#   soma a gravidade em
    rn[2, 0] = np.arcsin((U[0, 0]*sin(r[4, 0]) - U[1, 0]* cos(r[4, 0])) / np.linalg.norm(U))
#    computa as taxas de variacao
    rn[1, 0] = (rn[0, 0]-r[0, 0])/dt
    rn[3, 0] = (rn[2, 0]-r[2, 0])/dt
    return rn[0:4, 0]


def controle2giro(U):
    _U = U[0:3, 0] + g
    Tb = np.linalg.norm(_U)
    return np.sqrt(OMEGA * np.matrix([Tb, U[3, 0], U[4, 0], U[5, 0]]).T)




def rotacao(X):
# =============================================================================
# Entrada: Vetores de estados, Saida: matrix de rotacao que leva os
#    vetores no sistema do copor x para o sistema inercial X
# =============================================================================
    return np.matrix([[cos(X[10,0])*cos(X[8,0]), cos(X[10,0])*sin(X[6,0])*sin(X[8,0])-cos(X[6,0])*sin(X[10,0]),
                sin(X[6,0])*sin(X[10,0])+cos(X[6,0])*cos(X[10,0])*sin(X[8,0]) , ],
               [cos(X[8,0])*sin(X[10,0]), cos(X[6,0])*cos(X[10,0])+sin(X[6,0])*sin(X[10,0])*sin(X[8,0]) ,cos(X[6,0])*sin(X[10,0])*sin(X[8,0])-cos(X[10,0])*sin(X[6,0])],
               [-sin(X[8,0]),cos(X[8,0])*sin(X[6,0]) ,cos(X[6,0])*cos(X[8,0])]])

