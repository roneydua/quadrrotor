import sys
# import numpy as np
import sympy as sp
from IPython.core.display import Image, display


if True:
    sys.path.append("/home/roney/Dropbox/pacotesPython")
    from QuatSymbolic import*
    sp.init_session(ipython=True, use_latex=True)
    sp.init_printing(backcolor="White")


def sl(a):
    print(sp.latex(a))


Ixx, Iyy, Izz, I0, II = sp.symbols(
    r'I_{x}, I_{y}, I_{z}, I_0, \mathcal{I}', Real=True, Positive=True)  # [m Ixx Iyy Izz]

w1, w2, w3 = sp.symbols(r'\omega_{1}, \omega_{2}, \omega_{3} ')

# t = sp.var('t')
# fi = sp.Function(r'\phi')(t)
# theta = sp.Function(r'\theta')(t)
# psi = sp.Function(r'\psi')(t)

# fi_p = D(fi, t)
# theta_p = D(theta, t)
# psi_p = D(psi, t)
# eta = sp.Matrix([0, fi, theta, psi])
# eta_p = sp.Matrix([fi_p, theta_p, psi_p])

q = QuaternionSymbolic()
dq = QuaternionSymbolic("diff")
wq = QuaternionSymbolic()
wq.quat[0] = 0
wq.quat[1] = w1
wq.quat[2] = w2
wq.quat[3] = w3
w = 2 * Q.T * dq.quat
Iq = sp.Matrix(sp.diag(I0, Ixx, Iyy, Izz))
I = sp.Matrix(sp.diag(Ixx, Ixx, (II+1)*Ixx))

S = q.matrixSS()
Sd = dq.matrixSS()
Q = q.matrixS()
Qd = dq.matrixS()
# M = 4 * S * (Iq * S.T)
# Minv = S * (Iq**-1) * S.T / 4
# W = wq.matrixS()
Wtil = wq.matrixS()[1:, :]
matrixA = (Q * I**-1 * Wtil * I * wq.quat[1:, 0]/2).expand()
matrixA_q = (matrixA - (dq.quat.T * dq.quat)[0, 0]*q.quat).expand()
for i in range(4):
    matrixA_q[i, 0] = matrixA_q[i, 0].cancel()
    matrixA_q[i, 0] = matrixA_q[i, 0].subs(
        {w1: w[0], w2: w[1], w3: w[2]}).expand()


def getCoefficient(var, lin):
    t = sp.collect(matrixA_q[lin].expand(), var, evaluate=False)[var]
    t
    return t


combinationVars = [dq.quat[0]**2, dq.quat[0] * dq.quat[1], dq.quat[0] *
                   dq.quat[2], dq.quat[0] * dq.quat[3], dq.quat[1] ** 2,
                   dq.quat[1] * dq.quat[2], dq.quat[1] * dq.quat[3],
                   dq.quat[2]**2, dq.quat[2] * dq.quat[3], dq.quat[3] **
                   2]
# combinationVars = [q.quat[0]**2, q.quat[0] * q.quat[1], q.quat[0] *
#                    q.quat[2], q.quat[0] * q.quat[3], q.quat[1] ** 2,
#                    q.quat[1] * q.quat[2], q.quat[1] * q.quat[3],
#                    q.quat[2]**2, q.quat[2] * q.quat[3], q.quat[3] **
#                    2]

C = sp.Matrix(sp.zeros(4, 10))
for lin in range(4):
    for col in range(10):
        #print('elemento '+str(1+lin)+','+str(col))
        C[lin, col] = getCoefficient(
            combinationVars[col], lin).cancel().collect(II).collect(q.quat)
        # find if expoent major that 2
        maxDegree = max(sp.degree_list(C[lin, col], q.quat[0:4]))
        if maxDegree == 3:
            index = (sp.degree_list(C[lin, col], q.quat[0:4])).index(3)
            # print('Elemento '+str(lin)+str(col)+' no indice '+str(index))
            varTemp = 1 - (q.quat.T * q.quat)[0, 0] + q.quat[index]**2
            C[lin, col] = C[lin, col].subs(
                {q.quat[index]**3: q.quat[index]*varTemp}).expand()
        C[lin, col] = sp.collect(C[lin, col], -2*II)
        varTemp = C[lin, col].collect(II, evaluate=False)
        if II in varTemp:
            varTemp = (varTemp[II]/2).expand()
            varTemp2 = (C[lin, col] - 2 * II * varTemp).expand()
            C[lin, col] = (varTemp2.collect(
                q.quat)).factor(deep=True) + (2 * II * sp.collect(varTemp, q.quat))
        else:
            C[lin, col] = C[lin, col].factor(deep=True)


for lin in range(4):
    for col in range(10):
        if C[lin, col] != 'None':
            print('C_{'+str(lin)+','+str(col)+'} & =' +
                  sp.latex(C[lin, col])+'\\\\')
            # sp.latex(C[lin, col].collect(II).collect(q.quat))+'\\\\')
        else:
            print('C_{'+str(lin)+','+str(col)+'} & ='+str(0))


def printMatrixState():
    mTemp = sp.Matrix(sp.zeros(4, 1))
    for lin in range(4):
        for col in range(10):
            coefciente = sp.Symbol('C_{'+str(lin)+','+str(col)+'} ')
            mTemp[lin, 0] += coefciente * combinationVars[col]
    sl(mTemp)


printMatrixState()
