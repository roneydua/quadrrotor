import sys
# import numpy as np
import pickle
import sympy as sp
from IPython.core.display import Image, display

if True:
    # sys.path.append("/home/roney/Dropbox/pacotesPython")
    from QuatSymbolic import*
    sp.init_session(ipython=True, use_latex=True)
    sp.init_printing(backcolor="White")


def sl(a):
    print(sp.latex(a))


Ixx, Iyy, Izz, I0, II, grav, m = sp.symbols(
    r'I_{x}, I_{y}, I_{z}, I_0, \mathcal{I}, g, m', Real=True, Positive=True)  # [m Ixx Iyy Izz]
Tb, M_theta, M_fi, M_psi = sp.symbols(
    r'T_b, M_{\theta}, M_{\phi}, M_{\psi}', Real=True, Positive=True)  # [m Ixx Iyy Izz]
U1, U2, U3 = sp.symbols(r'U_{1}, U_{2}, U_{3}',
                        Real=True)  # [m Ixx Iyy Izz]
# controles virtuais
Uv = sp.Matrix([U1, U2, U3])
# vetor de forcas generalizadas translacionais e rotaciais
Ft_b = sp.Matrix([0, 0, Tb/m])
Fr_b = sp.Matrix([M_fi, M_theta, M_psi])
vg = sp.Matrix([0, 0, grav])

phi, theta, psi = sp.symbols(r'\phi \theta \psi')
# t = sp.var('t')
# fi = sp.Function(r'\phi')(t)
# theta = sp.Function(r'\theta')(t)
# psi = sp.Function(r'\psi')(t)

# fi_p = D(fi, t)
# theta_p = D(theta, t)
# psi_p = D(psi, t)
# eta = sp.Matrix([0, fi, theta, psi])
# eta_p = sp.Matrix([fi_p, theta_p, psi_p])

q = QuaternionSymbolic()
dq = QuaternionSymbolic(diff=True, sub=" ")

# %%

Iq = sp.Matrix(sp.diag(I0, Ixx, Iyy, Izz))
I = sp.Matrix(sp.diag(Ixx, Iyy, Izz))
# I = sp.Matrix(sp.diag(Ixx, Iyy, Izz))
S = q.S()
Sd = dq.S()
Q = q.Q()
Qd = dq.Q()
# M = 4 * S * (Iq * S.T)
# Minv = S * (Iq**-1) * S.T / 4
# Qc = sp.diag(Ixx, Iyy, Izz)
Q_Qd = Q.T * Qd
Q_Qd[0, 0] = 0
Q_Qd[1, 1] = 0
Q_Qd[2, 2] = 0
# matrixA = -2*(Q * I**-1 * Q_Qd * I * Qd.T).expand()
matrixA = -2*(Q * I**-1 * Q_Qd * I * Qd.T)
# matrixA_q = (matrixA * q.quat - (dq.quat.T * dq.quat)[0, 0]*q.quat).expand()
matrixA_q = (matrixA * q.quat - (dq.quat.T * dq.quat)[0, 0]*q.quat)
# for i in range(4):
#     matrixA_q[i, 0] = matrixA_q[i, 0].cancel()

# %%

# position = 3
# tempVar = q.quat[position]*dq.quat[position]
# tempVar2 = tempVar - q.quat.dot(dq.quat)
# matrixA_q = matrixA_q.subs(tempVar, tempVar2).expand()


def getCoefficient(var, lin):
    t = sp.collect(matrixA_q[lin].expand(), var, evaluate=False)[var]
    return t


combinationVarsMatrix = sp.Matrix([dq.quat[0]**2, dq.quat[0] * dq.quat[1], dq.quat[0] *
                                   dq.quat[2], dq.quat[0] *
                                   dq.quat[3], dq.quat[1] ** 2,
                                   dq.quat[1] *
                                   dq.quat[2], dq.quat[1] * dq.quat[3],
                                   dq.quat[2]**2, dq.quat[2] * dq.quat[3], dq.quat[3] **
                                   2])

combVars = [dq.quat[0]**2, dq.quat[0] * dq.quat[1], dq.quat[0] *
            dq.quat[2], dq.quat[0] * dq.quat[3], dq.quat[1] ** 2,
            dq.quat[1] * dq.quat[2], dq.quat[1] * dq.quat[3],
            dq.quat[2]**2, dq.quat[2] * dq.quat[3], dq.quat[3] **
            2]

C = sp.Matrix(sp.zeros(4, 10))
for lin in range(4):
    for col in range(10):
        #print('elemento '+str(1+lin)+','+str(col))
        C[lin, col] = getCoefficient(combVars[col], lin)
        matrixA_q[lin] = (matrixA_q[lin] - C[lin, col]*combVars[col]).expand()

        # find if expoent major that 2
        # maxDegree = max(sp.degree_list(C[lin, col], q.quat[0:4]))
        # if maxDegree == 3:
        #     index = (sp.degree_list(C[lin, col], q.quat[0:4])).index(3)
        #     # print('Elemento '+str(lin)+str(col)+' no indice '+str(index))
        #     varTemp = 1 - (q.quat.T * q.quat)[0, 0] + q.quat[index]**2
        #     C[lin, col] = C[lin, col].subs(
        #         {q.quat[index]**3: q.quat[index]*varTemp}).expand()
        # C[lin, col] = sp.collect(C[lin, col], -2*II)
        # varTemp = C[lin, col].collect(II, evaluate=False)
        # if II in varTemp:
        #     varTemp = (varTemp[II]/2).expand()
        #     varTemp2 = (C[lin, col] - 2 * II * varTemp).expand()
        #     C[lin, col] = (varTemp2.collect(
        #         q.quat)).factor(deep=True) + (2 * II * sp.collect(varTemp, q.quat))
        # else:
        #     C[lin, col] = C[lin, col].factor(deep=True)


# %%
# second stage of simplification
c1, c2, c3 = sp.symbols('c_1, c_2, c_3', Real=True)
C2 = 0 * C
varTemp0 = 1 - (q.quat.T * q.quat)[0, 0] + q.quat[0]**2
for lin in range(4):
    for col in range(10):
        C2[lin, col] = C[lin, col].subs(
            q.quat[1]*q.quat[3], -c1/(2*II)-q.quat[0]*q.quat[2]).expand()
        C2[lin, col] = C2[lin, col].subs(
            q.quat[0]*q.quat[1], c2/(2*II)+q.quat[2]*q.quat[3]).expand()
        C2[lin, col] = C2[lin, col].subs({q.quat[0]**2: varTemp}).expand()
        C2[lin, col] = C2[lin, col].subs(
            {q.quat[1]**2: c3/(2*II)-q.quat[2]**2}).expand()
        C2[lin, col] = C2[lin, col].subs(
            q.quat[1]*q.quat[3], -c1/(2*II)-q.quat[0]*q.quat[2]).expand()
        C2[lin, col] = C2[lin, col].collect(q.quat)

B = (((Q * I**-1 / 2)).expand())
''' save variables'''
save_data = [C2, B, q, dq, Q, I]

with open('quaternionVariables.pickle', 'w') as f:
    f.dump(save_data, f)


# C2 = 0 * C
# for lin in range(4):
#     for col in range(10):
#         C2[lin, col] = C[lin, col].subs(
#             q.quat[1]*q.quat[3], -a-q.quat[0]*q.quat[2]).expand()
#         C2[lin, col] = C2[lin, col].subs(
#             q.quat[0]*q.quat[1], b+q.quat[2]*q.quat[3]).expand()
#         C2[lin, col] = C2[lin, col].subs({q.quat[0]**2: varTemp}).expand()
#         C2[lin, col] = C2[lin, col].subs(
#             {q.quat[1]**2: c-q.quat[2]**2}).expand()
#         C2[lin, col] = C2[lin, col].subs(
#             q.quat[1]*q.quat[3], -a-q.quat[0]*q.quat[2]).expand()
#         C2[lin, col] = (C2[lin, col]) / 2/II
#         C2[lin, col] = (C2[lin, col]).expand()
#         C2[lin, col] = C2[lin, col].collect(q.quat)


for lin in range(2):
    for col in range(10):
        if C2[lin, col] != 'None':
            print('C_{'+str(lin)+','+str(col)+'} & =' +
                  sp.latex(C2[lin, col])+'\\\\')
            # sp.latex(C[lin, col].collect(II).collect(q.quat))+'\\\\')
        else:
            print('C_{'+str(lin)+','+str(col)+'} & ='+str(0))


def printsystem():
    # print('\\left[\\begin{matrix}')
    for lin in range(4):
        text = '\\ddot{q}_'+str(lin)+'& ='
        for col in range(10):
            text += 'C_{'+str(lin)+','+str(col)+'}'+sp.latex(combVars[col])+'+'
        text += '\\\\'
        print(text)
        print()
        print()
    # print('\\left[\\end{matrix}')


printsystem()


Rq_b_i = q.quatRot(1)  # Rotation matrix of quaternion q_b_i
systeTranslacional = Rq_b_i.T * (Uv+vg)
(systeTranslacional[0] + systeTranslacional[1]).expand()

'''Trecho para computo do quaternion apartir dos anguolos de Euler
!!! os computos sao feitos com os conjugados dos quaternios!!!
'''
qpsi = QuaternionSymbolic(sub='',coord=[sp.cos(psi/2), 0, 0, sp.sin(psi/2)])
qtheta = QuaternionSymbolic( coord=[sp.cos(theta/2), 0, sp.sin(theta/2), 0])
qphi = QuaternionSymbolic(coord=[sp.cos(phi/2), sp.sin(phi/2), 0, 0])
qtotal = QuaternionSymbolic()
qtotal.quat = (qtheta.multQuat(qphi))
qtotal.quat = qpsi.multQuat(qtotal)

# qtotal.quat = (qtheta.multQuat(qpsi))
# qtotal.quat = qphi.multQuat(qtotal)

Rtotal = qtotal.quatRot(1)
for i in range(3):
    for j in range(3):
        Rtotal[i, j] = Rtotal[i, j].trigsimp()

Rtotal
del qpsi, qtheta, qphi, qtotal, Rtotal
'''Fim do trecho dos angulos de Euler'''


'''Trecho da analise do quaternion de controle'''
qpsi = QuaternionSymbolic(coord=[sp.cos(psi/2), 0, 0, sp.sin(psi/2)])
# qtheta = QuaternionSymbolic(coord=[sp.cos(theta/2), 0, sp.sin(theta/2), 0])
# qphi = QuaternionSymbolic(coord=[sp.cos(phi/2), sp.sin(phi/2), 0, 0])
Rpsi = qpsi.quatRot(1).T
for i in range(len(Rpsi)):
    Rpsi[i] = Rpsi[i].trigsimp(method='fu')

U1, U2, U3 = sp.symbols(r'U_1, U2, U3')
Uv = sp.Matrix([U1, U2, U3])


sl((Rpsi*(Uv+vg) * m/Tb+vg/grav)/2)
qc = QuaternionSymbolic(sub='c')
qc.quat[0] = sp.sqrt(Tb+m*(U3+grav))
qc.quat[1] = m*(U1*sin(psi)-U2*cos(psi))/qc.quat[0]
qc.quat[2] = m*(U2*sin(psi)+U1*cos(psi))/qc.quat[0]
qc.quat[3] = 0
qc.quat /=sp.sqrt(2*Tb)

qpsiConj = qpsi.conj()

qe = QuaternionSymbolic()
for i in range(4):
    qe.quat[i]=qpsiConj.multQuat(qc)[i].trigsimp(method='fu').factor()
sl(qe.quat)



# %% verification of controlability
matrixA = Q*I**-1*Q_Qd*I*Q.T - q.quat*dq.quat.T
A = sp.zeros(11, 11)
B = sp.zeros(11, 7)
B[:3, :3] = sp.eye(3)
A[3, 7] = 1
A[4, 8] = 1
A[5, 9] = 1
A[6, 10] = 1
A[-4:, -4:] = matrixA
B[-4:, -3:] = (((Q * I**-1 / 2)).expand())
B[-4:, -4:] = (Q * I**-1 * Q.T)/4

M = sp.Matrix(BlockMatrix([[B, A*B, A*A*B]]).as_explicit())
M = M.subs(dq.quat[0], 0)
M = M.subs(dq.quat[1], 0)
M = M.subs(dq.quat[2], 0)
M = M.subs(dq.quat[3], 0)
M.rank()
