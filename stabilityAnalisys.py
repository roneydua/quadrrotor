import sys
# import numpy as np
import pickle
import sympy as sp
from IPython.core.display import Image, display

if True:
    sys.path.append("/home/roney/Dropbox/pacotesPython")
    from QuatSymbolic import*
    sp.init_session(ipython=True, use_latex=True)
    sp.init_printing(backcolor="White")


def sl(a):
    print(sp.latex(a))


Ixx, Iyy, Izz, I0, II, grav, m = sp.symbols(
    r'I_{x}, I_{y}, I_{z}, I_0, \mathcal{I}, g, m', Real=True, Positive=True)  # [m Ixx Iyy Izz]
Tb, M_theta, M_fi, M_psi = sp.symbols(
    r'T_b, M_{\theta}, M_{\phi}, M_{\psi}', Real=True, Positive=True)  # [m Ixx Iyy Izz]
U1, U2, U3 = sp.symbols(r'U_{1}, U_{2}, U_{3}',
                        Real=True)  # [m Ixx Iyy Izz]
# controles virtuais
Uv = sp.Matrix([U1, U2, U3])
# vetor de forcas generalizadas translacionais e rotaciais
Ft_b = sp.Matrix([0, 0, Tb/m])
Fr_b = sp.Matrix([M_fi, M_theta, M_psi])
vg = sp.Matrix([0, 0, grav])

phi, theta, psi = sp.symbols(r'\phi \theta \psi')

save_data = [C2, B, q, dq, Q, I, S, combinationVarsMatrix]


with open('quaternionVariables.pickle', 'rb') as f:
    C2, BB, q, dq, Q, I, S, x = pickle.load(f)


A = sp.Matrix(sp.zeros(8, 8))
B = sp.Matrix(sp.zeros(8, 3))
A[0, 1] = 1
A[2, 3] = 1
A[4, 5] = 1
A[6, 7] = 1

x = sp.Matrix(sp.zeros(8, 1))
for col in range(1, 8, 2):
    A[1, col] = sp.Symbol('a_{1,'+str(col)+'}')
    A[3, col] = sp.Symbol('a_{3,'+str(col)+'}')
    A[5, col] = sp.Symbol('a_{5,'+str(col)+'}')
    A[7, col] = sp.Symbol('a_{7,'+str(col)+'}')

for col in range(1, 8, 2):
    A[1, col] = sp.Symbol('a_{1,'+str(col)+'}')
    A[3, col] = sp.Symbol('a_{3,'+str(col)+'}')
    A[5, col] = sp.Symbol('a_{5,'+str(col)+'}')
    A[7, col] = sp.Symbol('a_{7,'+str(col)+'}')

for lin in range(4):
    B[2*lin+1, 0] = (2*Ixx*BB[lin, 0]).simplify()
    B[2*lin+1, 1] = (2*Ixx*BB[lin, 1]).simplify()
    B[2*lin+1, 2] = (2*Ixx*BB[lin, 2]).simplify()
    x[2*lin, 0] = q.quat[lin]
    x[2*lin+1, 0] = dq.quat[lin]

M = sp.Matrix(sp.BlockMatrix([[B, A*B, A*A*B, A*A*A*B]]).as_explicit())


'''analise com coeficientes'''
for i in range(4):
    A[2*i+1, 1] = C2[i, 0]*dq.quat[0]+C2[i, 1]*dq.quat[1]
    A[2*i+1, 3] = C2[i, 4]*dq.quat[1]+C2[i, 5]*dq.quat[2]
    A[2*i+1, 5] = C2[i, 2]*dq.quat[0]+C2[i, 7]*dq.quat[2]
    A[2*i+1, 7] = C2[i, 3]*dq.quat[0]+C2[i, 6]*dq.quat[1] + \
        C2[i, 8]*dq.quat[2]+C2[i, 9]*dq.quat[3]


for lin in range(M.rows):
    for col in range(M.cols):
        for k in range(4):
            M[lin, col] = M[lin, col].subs({dq.quat[k]: 0})
        # M[lin, col] = M[lin, col].subs({q.quat[3]: sp.sqrt(1-(q.quat.T*q.quat)[0,0]+q.quat[3]*q.quat[3])})
M
