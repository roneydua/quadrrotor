import numpy as np
import sys
from numpy import cos, sin, sqrt, matrix
from numpy.linalg import norm
import matplotlib.pyplot as plt
import importlib
import Drone
import sympy as sp
from IPython.core.display import Image, display
from QuatSymbolic import *
if True:
#     sys.path.append("/home/roney/Dropbox/pacotesPython")
#     from QuatSymbolic import *
    # sp.init_session(ipython=True, use_latex=True)
    sp.init_printing(backcolor="White")


importlib.reload(Drone)


def sl(a):
    print(sp.latex(a))


dt = sp.Symbol('\delta t')
# if True:
#     sys.path.append("/home/roney/Dropbox/pacotesPython")
#     from QuatSymbolic import*


# salva dados
''' Analise da solucao de Riccati'''
q = QuaternionSymbolic()
dq = QuaternionSymbolic(diff=True)
q_des = QuaternionSymbolic(sub='d')
dq_des = QuaternionSymbolic(diff=True, sub='d')
q_e = QuaternionSymbolic(sub='e_{k}')
dq_e = QuaternionSymbolic(diff=True, sub='e_{k}')

R = sp.Matrix(sp.zeros(6, 6))

quadSymb = Drone.DorneSymbolic()
quadSymb.Am()


def reecreva(R, name):
    for row in range(R.rows):
        for col in range(R.cols):
            if R[row, col] != 0:
                R[row, col] = sp.Symbol(name+'_{'+str(row)+str(col)+'}')
    return R


def simbolicMatrix(R, name, diagonal=False):
    if diagonal == True:
        for diag in range(R.rows):
            R[diag, diag] = sp.Symbol(name+'_{'+str(diag)+str(diag)+'}')
        return R
    else:
        for row in range(R.rows):
            for col in range(row, R.cols):
                R[row, col] = sp.Symbol(name+'_{'+str(row)+str(col)+'}')
                R[col, row] = sp.Symbol(name+'_{'+str(row)+str(col)+'}')
        return R


Q = sp.Matrix(sp.zeros(9, 9))
Q = simbolicMatrix(Q, 'Q', diagonal=True)
R = simbolicMatrix(R, 'R', diagonal=True)

E = simbolicMatrix(0*Q, 'E', diagonal=True)
E = quadSymb.Bm() * R**-1 * quadSymb.Bm().T

# analise do erro

# q_e.quat = (q_des.S().T) * q.quat.subs(q_des.quat[0], sp.sqrt(1-q_des.quat[1:,0].dot(q_des.quat[1:,0])))

f_q0 = sp.Function(r'f')(q.quat[1], q.quat[2], q.quat[3])
# f_q0 = sp.Function(r'f')
f_q0 = sp.sqrt(1-q.quat[1:,0].dot(q.quat[1:,0]))
# q_e.quat = q_des.S().T * q.quat.subs(q.quat[0], f_q0)
# q_e.quat = q.S(right=1).subs(q.quat[0],sp.sqrt(1-q.quat[1:,0].dot(q.quat[1:,0]))) * q_des.quat
#   q.S(right=1) * q_des.quat
I3 = sp.eye(3)
C1 = sp.eye(9)
C1[3:6, 3:6] *= -f_q0
C2 = sp.eye(9)
C2[3:6, 3:6] = q_des.screw() - q_des.quat[0]*I3


Xd, Yd, Zd, X, Y, Z = sp.symbols(r'\dot{X}_d, \dot{Y}_d, \dot{Z}_d,\dot{X}, \dot{Y}, \dot{Z}')
w = sp.Matrix([sp.symbols(r'omega_1'), sp.symbols(r'omega_2'), sp.symbols(r'omega_3')])
wd = sp.Matrix([sp.symbols(r'omega_1_d'), sp.symbols(r'omega_2_d'), sp.symbols(r'omega_3_d')])


e = sp.Matrix([Xd-X, Yd-Y, Zd-Z, q_e.quat[1], q_e.quat[2], q_e.quat[3], wd[0]-w[0], wd[1]-w[1], wd[2]-w[2]])

state = sp.Matrix([X, Y, Z, q.quat[1], q.quat[2], q.quat[3], w[0], w[1], w[2]])
stateDesire = sp.Matrix([Xd, Yd, Zd, q_des.quat[1], q_des.quat[2], q_des.quat[3], wd[0], wd[1], wd[2]])

erroTotal = (stateDesire[:3, 0]- state[:3, 0]).T * Q[:3, :3] * (stateDesire[:3, 0]- state[:3, 0])
erroTotal += (stateDesire[6:, 0] - state[6:, 0]).T * Q[6:, 6:] * (stateDesire[6:, 0] - state[6:, 0])
erroTotal
q_e.quat[1:, 0] = (q_des.quat[0,0] * I3-q_des.screw())*q.vec() - f_q0 * q_des.vec()


erroTotal += q_e.quat[1:, 0].T * Q[3:6, 3:6] * q_e.quat[1:, 0]
e = (C1 * stateDesire - C2 * state).subs(f_q0, q.quat[0, 0])
# sl(erroTotal[0,0])

# f_q0 = sp.sqrt(1-q.quat[1:, 0].dot(q.quat[1:, 0]))
# f_q0_des = sp.sqrt(1-q_des.quat[1:, 0].dot(q_des.quat[1:, 0]))
def derive(t, subs_q0=False):
    diffErroTotal = sp.zeros(len(state), 1)
    for i in range(len(state)):
        diffErroTotal[i] = sp.expand(sp.diff(t, state[i]))
    if subs_q0==False:
        return diffErroTotal
    else:
        return diffErroTotal.subs(f_q0, q.quat[0])

-derive(erroTotal,subs_q0=1)/2
Q35 = Q[3:6, 3:6]
exato1 = derive(stateDesire.T * C1.T * Q * C1 * stateDesire, subs_q0=True)[3:6, 0]

exato2 = 2*derive(state.T*C2.T*Q*C1*stateDesire, subs_q0=1)[3:6, 0]

exato3 = derive(state.T * C2.T * Q*C2*state, subs_q0=1)[3:6, 0]

(derive(erroTotal, subs_q0=1)[3:6,0] - (exato1 - exato2 + exato3)).expand()

simplificado1  =( -2 * (q_des.vec().T * Q35 * q_des.vec())[0,0]*q.vec()).expand()


simplificado2 = 2*(q.quat[0, 0] * (q_des.quat[0,0]*I3 + q_des.screw()) * Q35*q_des.vec() + 1/q.quat[0,0]*(q_des.vec().T* Q35 *(-q_des.quat[0,0]*I3+q_des.screw())*q.vec())[0,0]*q.vec()).expand()


# verificando terceiro termo
simplificado3 =(2*(q_des.quat[0,0]*I3+q_des.screw()) * Q35*(q_des.quat[0,0]*I3-q_des.screw()) * q.vec()).expand()

(exato1 - exato2 + exato3) - (simplificado1 - simplificado2 + simplificado3)

a = (q_des.quat[0,0] * I3 +q_des.screw())*  Q35 * (q_des.quat[0,0] * I3 -q_des.screw())
b = q_des.quat[0,0]**2 * Q35 + q_des.quat[0,0]*q_des.screw()*Q35 -q_des.quat[0,0]*Q35*q_des.screw() - q_des.screw()*Q35*q_des.screw()
c = q_des.quat[0,0]**2 * Q35 + 2*q_des.quat[0,0]*q_des.screw()*Q35 - q_des.screw()*Q35*q_des.screw()
(a-c).expand()


q_des.quat[0,0]*q_des.screw()*Q35
(q_des.quat[0,0]*Q35*q_des.screw()).T
q_des.quat[0,0]*q_des.screw()*Q35


# %% solucao algebrica de riccati
K = sp.Matrix(sp.zeros(9, 9))
K = simbolicMatrix(K, 'K')
A = 1*quadSymb.Am()
A = reecreva(A, 'A')
E = quadSymb.Bm() * R**-1 * quadSymb.Bm().T
E = reecreva(E, 'E')
V = simbolicMatrix(K*0, 'V')
V[3:, :3] = sp.zeros(6, 3)
V[:3, 3:] = sp.zeros(3, 6)
V[6:,3:6] = sp.zeros(3, 3)
V[3:6,6:] = sp.zeros(3, 3)
V[:3,:3] = Q[:3,:3]
V[6:,6:] = Q[6:,6:]
K * A + A.T * K - K * E * K + V

# %%
kf, km, L = sp.symbols('k_{f}, k_{m}, L', Real= True, Positive=True)
m = sp.Matrix([[kf, kf, kf, kf],
           [0, L*kf, 0, -L*kf],
           [-L*kf, 0, L*kf, 0],
           [-km, km, -km, km]])
sl(m**-1)
sl(m.det())
