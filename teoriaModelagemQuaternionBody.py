import sys
# import numpy as np
import pickle
import sympy as sp
from IPython.core.display import Image, display

if True:
    from QuatSymbolic import*
    # sys.path.append("/home/roney/Dropbox/pacotesPython")
    sp.init_session(ipython=True, use_latex=True)
    sp.init_printing(backcolor="White")


def sl(a):
    print(sp.latex(a))

# %%
Ixx, Iyy, Izz, I0, II, grav, m = sp.symbols(
    r'I_{x}, I_{y}, I_{z}, I_0, \mathcal{I}, g, m', Real=True, Positive=True)  # [m Ixx Iyy Izz]
Tb, M_y, M_x, M_z = sp.symbols(
    r'T_b, M_{y}, M_{x}, M_{z}', Real=True, Positive=True)  # [m Ixx Iyy Izz]
U1, U2, U3 = sp.symbols(r'U_{1}, U_{2}, U_{3}',
                        Real=True)  # [m Ixx Iyy Izz]
wb = sp.Matrix([sp.Symbol('\omega_1'), sp.Symbol(
    '\omega_2'), sp.Symbol('\omega_3')])
wb_q = QuaternionSymbolic(coord=[0, wb[0, 0], wb[1, 0], wb[2, 0]])
wb_q.screw()
# controles virtuais
Uv = sp.Matrix([U1, U2, U3])
# vetor de forcas generalizadas translacionais e rotaciais
Ft_b = sp.Matrix([0, 0, Tb/m])
Fr_b = sp.Matrix([M_x, M_y, M_z])
vg = sp.Matrix([0, 0, grav])

phi, theta, psi = sp.symbols(r'\phi \theta \psi')

q = QuaternionSymbolic()
dq = QuaternionSymbolic(diff=True)
Q = q.S()

I = sp.Matrix(sp.diag(Ixx, Iyy, Izz))

# sl(I**-1 * wb_q.screw() * I * wb_q.quat[1:,0] + I**-1 * Fr_b)

# I = sp.Matrix(sp.diag(Ixx, Iyy, Izz))
# %%

def getCoefficient(var, lin):
    t = sp.collect(matrixA_q[lin].expand(), var, evaluate=False)[var]
    # t
    return t

def verifyControlability(A, B, n):
    lA = 1*A
    lB = 1*B
    lA.col_del(n)
    lA.row_del(n)
    lB.row_del(n)
    M = sp.Matrix(BlockMatrix([[lB, lA*lB]]).as_explicit())
    M.col_del(6)
    M.col_del(6)
    M.col_del(6)
    return M.det()



A = sp.Matrix(sp.zeros(10, 10))
B = sp.Matrix(sp.zeros(10, 6))

A[3:7, 7:] = q.Q() / 2
A[7, 7] = 0
A[7, 8] = -Iyy*wb[2]/Ixx
A[7, 9] = Izz*wb[1]/Ixx
A[8, 7] = Ixx*wb[2]/Iyy
A[8, 8] = 0
A[8, 9] = -Izz*wb[0]/Iyy
A[9, 7] = -Ixx*wb[1]/Izz
A[9, 8] = Iyy*wb[0]/Izz
A[9, 9] = 0
B[:3,:3] = sp.eye(3)
# B[-3:,-4:] = I**-1 * q.Q().T
B[7, 3] = 1/Ixx
B[8, 4] = 1/Iyy
B[9, 5] = 1/Izz

norm = q.norm()

(verifyControlability(A, B, 3).subs(q.quat[1]**2,1-norm+q.quat[1]**2)).expand()
(verifyControlability(A, B, 4).subs(q.quat[0]**2,1-norm+q.quat[0]**2)).expand()
(verifyControlability(A, B, 5).subs(q.quat[0]**2,1-norm+q.quat[0]**2)).expand()
(verifyControlability(A, B, 6).subs(q.quat[0]**2,1-norm+q.quat[0]**2)).expand()


M = sp.Matrix(BlockMatrix([[B, A*B, A*A*B, A*A*A*B,A*A*A*A*B,A*A*A*A*A*B]]).as_explicit())
M.rank()



# %%

for lin in range(M.rows):
    for col in range(M.cols):
        for k in range(3):
            M[lin, col] = M[lin, col].subs({wb[k]: 0})
        # M[lin, col] = M[lin, col].subs({q.quat[3]: sp.sqrt(1-(q.quat.T*q.quat)[0,0]+q.quat[3]*q.quat[3])})
M
'''del first element for confirm controlabilitie'''
# A.row_del(0)
# A.col_del(0)
# B.row_del(0)
M = sp.Matrix(BlockMatrix([[B, A*B]]).as_explicit())


for lin in range(M.rows):
    for col in range(M.cols):
        for k in range(3):
            M[lin, col] = M[lin, col].subs({wb[k]: 0})
        M[lin, col] = M[lin, col].subs(
            {q.quat[0]: sp.sqrt(1-(q.quat.T*q.quat)[0, 0]+q.quat[0]**2)})
M
sl(M.det())


'''Add translacional velocitie dynamics'''
Af = sp.BlockMatrix([[sp.Matrix(sp.zeros(3, 6))], [A]]).as_explicit()
Af = sp.BlockMatrix([[sp.Matrix(sp.zeros(9, 3)), Af]]).as_explicit()
Bf = sp.BlockMatrix([[sp.Matrix(sp.zeros(6, 3)), B]]).as_explicit()
Bf = sp.BlockMatrix([[sp.Matrix(sp.zeros(3, 3))], [Bf]]).as_explicit()
