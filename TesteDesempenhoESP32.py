import serial
import struct
import numpy as np
import RK
import os
import matplotlib.pyplot as plt
import calcMatrizes as cM
import time

conexao = serial.Serial(str(os.popen("ls /dev/ttyUSB*").read()[:-1]), 115200)

t = np.arange(0, 20, 0.1)
x = np.asmatrix(np.zeros((12, len(t))))
u = np.asmatrix(np.zeros((6, len(t))))


r = np.asmatrix(np.zeros(shape=(12, len(t))))
r[0, :] = 10.0 * (t/5+1 - np.exp(-t/5))
r[1, :] = 10.0 * (1.0/5 + np.exp(-t/5)/5)
r[2, :] = 0.0
r[3, :] = 0.0
r[4, :] = 1.0
r[5, :] = 0.0
r[10, :] = np.deg2rad(10)*t + np.deg2rad(5)
r[11, :] = np.deg2rad(10)


x[:, 0] = r[:, 0]

def f(_x, _u):
    return (A * _x + B * _u)


def enviaPacote(_r, _x):
    """ Funcao Pacote que envia os dados de rota a ser seguida e os estados
    propagados

    Args:
        _r (numpy.matrix): Rota atualizada, vetor 12x1.
        _x (numpy.matrix): Estados propagados, vetor 12x1.

    Returns:
        None.
    """

    # cria matriz para enviar dados via lista
    envia = []
    envia = [_r[i, 0] for i in range(12)]
    envia.extend([_x[i, 0] for i in range(12)])
    # envia dados para computo no microcontrolador
    conexao.write(struct.pack('24f', *envia))
    # time.sleep(0.1)


def recebePacote():
    # data = [conexao.read() for i in range(28)]
    recebido = struct.unpack('7f', conexao.read(28))
    # retorna u, r, x e dt (nesta sequencia)
    return (np.matrix(recebido[0:6]).T, recebido[-1])


t *= 0.0
# %%

for i in range(len(t)-1):
    A, B = cM.planta(x[:, i])
    enviaPacote(r[:, i], x[:, i])
    u[:, i], dt = recebePacote()
    # dt = 0.2
    x[:, i+1] = RK.ResolveEDForcado(f, x[:, i], u[:, i], dt)
    t[i+1] = t[i]+dt
    r[6:10, i+1] = cM.atualizaRota(r[6:, i], dt, u[0:3, i])
    i += 1
    # print(i)


# omega[:, i+1] = cM.controle2giro(u[:, i])


#f = open('100s.pckl', 'wb')
#pickle.dump([t,x,r], f)
#f.close()

#with open('100s.pckl', 'rb') as f:
#    t, x, r = pickle.load(f)
#    f.close()


#plot2d()


def plot2dErros():
    plt.plot(t, r[0, :].T-x[0, :].T)
#    plt.plot(t,, ':')
    plt.plot(t, r[2, :].T-x[2, :].T)
#    plt.plot(t,r[2,:].T, ':')
    plt.plot(t, r[4, :].T-x[4, :].T)
#    plt.plot(t,r[4,:].T, ':')

# %%
def plot2dPosicao():
    plt.figure(num=1)
    plt.subplot(311)
    p = plt.plot(t, x[0, :].T, label=r'$x$')
    plt.plot(t, r[0, :].T, p[-1].get_color(),linestyle='dotted', label=r'$r_x$')
    plt.legend()
    plt.subplot(312)
    p = plt.plot(t, x[2, :].T, label=r'$y$')
    plt.plot(t, r[2, :].T, p[-1].get_color(),linestyle='dotted', label=r'$r_y$')
    plt.legend()
    plt.subplot(313)
    p = plt.plot(t, x[4, :].T, label=r'$z$')
    plt.plot(t, r[4, :].T, p[-1].get_color(),linestyle='dotted', label=r'$r_z$')
    plt.legend()


# %%
def plot2dVelocidade():
    plt.figure(num=2)
    plt.subplot(311)
    p = plt.plot(t, x[1, :].T, label=r'$\dot{x}$')
    plt.plot(t, r[1, :].T, p[-1].get_color(),linestyle='dotted', label=r'$r_{\dot{x}}$')
    plt.legend()
    plt.subplot(312)
    p = plt.plot(t, x[3, :].T, label=r'$\dot{y}$')
    plt.plot(t, r[3, :].T, p[-1].get_color(),linestyle='dotted', label=r'$r_{\dot{y}}$')
    plt.legend()
    plt.subplot(313)
    p = plt.plot(t, x[5, :].T, label=r'$\dot{z}$')
    plt.plot(t, r[5, :].T, p[-1].get_color(),linestyle='dotted', label=r'$r_{\dot{z}}$')
    plt.legend()


# %%
def plot2dangulos():
    _r = np.rad2deg(r)
    _x = np.rad2deg(x)
    plt.figure(num=3)
    plt.subplot(311)
    p = plt.plot(t, _x[6, :].T, label=r'$\theta$')
    plt.plot(t, _r[6, :].T, p[-1].get_color(),linestyle='dotted', label=r'$r_{\theta}$')
    plt.legend()
    plt.subplot(312)
    p = plt.plot(t, _x[8, :].T, label=r'$\phi$')
    plt.plot(t, _r[8, :].T, p[-1].get_color(),linestyle='dotted', label=r'$r_{\phi}$')
    plt.legend()
    plt.subplot(313)
    p = plt.plot(t, _x[10, :].T, label=r'$\psi$')
    plt.plot(t, _r[10, :].T, p[-1].get_color(),linestyle='dotted', label=r'$r_{\psi}$')
    plt.legend()


# %%
def plot2dVelangulos():
    _r = np.rad2deg(r)
    _x = np.rad2deg(x)
    plt.figure(num=4)
    plt.subplot(311)
    p = plt.plot(t, _x[7, :].T, label=r'$\dot{\theta}$')
    plt.plot(t, _r[7, :].T, p[-1].get_color(),linestyle='dotted', label=r'$r_{\dot{\theta}}$')
    plt.legend()
    plt.subplot(312)
    p = plt.plot(t, _x[9, :].T, label=r'$\dot{\phi}$')
    plt.plot(t, _r[9, :].T, p[-1].get_color(),linestyle='dotted', label=r'$r_{\dot{\phi}}$')
    plt.legend()
    plt.subplot(313)
    p = plt.plot(t, _x[11, :].T, label=r'$\dot{\psi}$')
    plt.plot(t, _r[11, :].T, p[-1].get_color(),linestyle='dotted', label=r'$r_{\dot{\psi}}$')
    plt.legend()


def plotangulosErros():
    plt.plot(t,x[6,:].T-r[6,:].T)
    plt.plot(t,x[8,:].T-r[8,:].T)
    plt.plot(t,x[10,:].T-r[10,:].T)


def grafico3D(q):
    x = []
    y = []
    z = []
    for i in range(len(t)-1):
        x.append(q[0, i])
        y.append(q[1, i])
        z.append(q[2, i])
#        w.append(0*q[2, i])
    fig = plt.figure(num=1,figsize=(figlarg, figlarg/2),dpi=288)
    ax = fig.gca(projection='3d')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    ax.set_xlabel('x')
    plt.plot(x, y, z)
    ax.set_aspect('equal')
#    plt.legend([r'$\Delta\beta =$'+str(varbeta[k-1])])


# grafico3D(np.hstack((x[0,:].T,x[2,:].T,x[4,:].T)).T)
# grafico3D(np.hstack((r[0,:].T,r[2,:].T,r[4,:].T)).T)
# plt.legend(['Trajetoria quad.','Trajetoria desej.'])
# plt.plot(Omega.T)

# plotangulosErros()
# plt.figure()
plot2dPosicao()
plot2dVelocidade()
plot2dVelangulos()
plot2dangulos()
