import RK
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import Riccati
import calcMatrizes as cM
import control
import scipy
plt.style.use('/home/roney/Dropbox/pacotesPython/roney.mplstyle')

figlarg = 6
g = np.matrix([0, 0, 9.82]).T


#import pickle
dt = 0.05  # passo temporal
t = np.arange(0., 20., dt)  # vetor temporal
X = np.asmatrix(np.zeros(shape=(12, len(t))))  # vetor de estados [xp xs xt]
C, D = cM.calcmatrizesCD()
Q = np.matrix(np.eye(12))
#Q[1, 1] = 10.
#Q[3, 3] = 10.
#Q[5, 5] = 10.
#Q[10, 10] = 10.
R = np.matrix(np.eye(6))*10.0
altEquador = 6.371e6  # metros

# condicoes iniciais
# X[:, 0] = np.matrix([0.0, .0, 0.0, .0, 0., 0., 0., .0, .0, .0, .0, .0]).T
U = np.asmatrix(np.zeros((6, len(t))))  # vetor de entradas [Uv M]
omega = np.asmatrix(np.zeros((4, len(t))))
# matrizes globais
r = np.asmatrix(np.zeros(shape=(12, len(t))))
r[0, :] = 10.0 *(1.0 - np.exp(-t))
r[1, :] = 10 * np.exp(-t)
r[2, :] = 0.0
r[3, :] = 0.0
r[4, :] = 0.0
r[5, :] = 0.0
r[10, :] = 0.0*t #+ np.deg2rad(5)
r[11, :] = 0.0


# X[:, 0] = r[:, 0]
W = C.T * Q


def f(_x, _u):
    return (A * _x + B * _u)


# %%
for i in range(len(t)-1):
    A, B = cM.planta(X[:, i])
    RR = R**-1 * B.T
    E = B * RR
    P = Riccati.EARtrack(A, B, C, Q, R)
    K1 = -RR * P
    K2 = RR * ((P*E-A.T)**-1)*W
    U[:, i] = K1*X[:, i] + K2 * r[:, i]
    X[:, i+1] = RK.ResolveEDForcado(f, X[:, i], U[:, i], dt)
#    omega[:, i] = cM.controle2giro(U[:, i])
    r[6:10, i+1] = cM.atualizaRota(r[6:, i], dt, U[0:3, i])

# omega[:, i+1] = cM.controle2giro(U[:, i])


#f = open('100s.pckl', 'wb')
#pickle.dump([t,X,r], f)
#f.close()

#with open('100s.pckl', 'rb') as f:
#    t, X, r = pickle.load(f)
#    f.close()


#plot2d()



def plot2dErros():
    plt.plot(t,r[0,:].T-X[0,:].T)
#    plt.plot(t,, ':')
    plt.plot(t,r[2,:].T-X[2,:].T)
#    plt.plot(t,r[2,:].T, ':')
    plt.plot(t,r[4,:].T-X[4,:].T)
#    plt.plot(t,r[4,:].T, ':')


def plot2dPosicao():
    plt.figure(num=1)
    plt.subplot(311)
    p = plt.plot(t, X[0, :].T, label=r'$x$')
    plt.plot(t, r[0, :].T, p[-1].get_color(),linestyle='dotted', label=r'$r_x$')
    plt.legend()
    plt.subplot(312)
    p = plt.plot(t, X[2, :].T, label=r'$y$')
    plt.plot(t, r[2, :].T, p[-1].get_color(),linestyle='dotted', label=r'$r_y$')
    plt.legend()
    plt.subplot(313)
    p = plt.plot(t, X[4, :].T, label=r'$z$')
    plt.plot(t, r[4, :].T, p[-1].get_color(),linestyle='dotted', label=r'$r_z$')
    plt.legend()


def plot2dVelocidade():
    plt.figure(num=2)
    plt.subplot(311)
    p = plt.plot(t, X[1, :].T, label=r'$\dot{x}$')
    plt.plot(t, r[1, :].T, p[-1].get_color(),linestyle='dotted', label=r'$r_{\dot{x}}$')
    plt.legend()
    plt.subplot(312)
    p = plt.plot(t, X[3, :].T, label=r'$\dot{y}$')
    plt.plot(t, r[3, :].T, p[-1].get_color(),linestyle='dotted', label=r'$r_{\dot{y}}$')
    plt.legend()
    plt.subplot(313)
    p = plt.plot(t, X[5, :].T, label=r'$\dot{z}$')
    plt.plot(t, r[5, :].T, p[-1].get_color(),linestyle='dotted', label=r'$r_{\dot{z}}$')
    plt.legend()


def plot2dangulos():
    _r = np.rad2deg(r)
    _X = np.rad2deg(X)
    plt.figure(num=3)
    plt.subplot(311)
    p = plt.plot(t, _X[6, :].T, label=r'$\theta$')
    plt.plot(t, _r[6, :].T, p[-1].get_color(),linestyle='dotted', label=r'$r_{\theta}$')
    plt.legend()
    plt.subplot(312)
    p = plt.plot(t, _X[8, :].T, label=r'$\phi$')
    plt.plot(t, _r[8, :].T, p[-1].get_color(),linestyle='dotted', label=r'$r_{\phi}$')
    plt.legend()
    plt.subplot(313)
    p = plt.plot(t, _X[10, :].T, label=r'$\psi$')
    plt.plot(t, _r[10, :].T, p[-1].get_color(),linestyle='dotted', label=r'$r_{\psi}$')
    plt.legend()


def plot2dVelangulos():
    _r = np.rad2deg(r)
    _X = np.rad2deg(X)
    plt.figure(num=4)
    plt.subplot(311)
    p = plt.plot(t, _X[6, :].T, label=r'$\dot{\theta}$')
    plt.plot(t, _r[6, :].T, p[-1].get_color(),linestyle='dotted', label=r'$r_{\dot{\theta}}$')
    plt.legend()
    plt.subplot(312)
    p = plt.plot(t, _X[8, :].T, label=r'$\dot{\phi}$')
    plt.plot(t, _r[8, :].T, p[-1].get_color(),linestyle='dotted', label=r'$r_{\dot{\phi}}$')
    plt.legend()
    plt.subplot(313)
    p = plt.plot(t, _X[10, :].T, label=r'$\dot{\psi}$')
    plt.plot(t, _r[10, :].T, p[-1].get_color(),linestyle='dotted', label=r'$r_{\dot{\psi}}$')
    plt.legend()

def plotangulosErros():
    plt.plot(t,X[6,:].T-r[6,:].T)
    plt.plot(t,X[8,:].T-r[8,:].T)
    plt.plot(t,X[10,:].T-r[10,:].T)

def grafico3D(q):
    x = []
    y = []
    z = []
    for i in range(len(t)-1):
        x.append(q[0, i])
        y.append(q[1, i])
        z.append(q[2, i])
#        w.append(0*q[2, i])
    fig = plt.figure(num=1,figsize=(figlarg, figlarg/2),dpi=288)
    ax = fig.gca(projection='3d')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    ax.set_xlabel('x')
    plt.plot(x, y, z)
    ax.set_aspect('equal')
#    plt.legend([r'$\Delta\beta =$'+str(varbeta[k-1])])




#grafico3D(np.hstack((X[0,:].T,X[2,:].T,X[4,:].T)).T)
#grafico3D(np.hstack((r[0,:].T,r[2,:].T,r[4,:].T)).T)
#plt.legend(['Trajetoria quad.','Trajetoria desej.'])
#plt.plot(Omega.T)
#
#plotangulosErros()
#plt.figure()
plot2dPosicao()
plot2dVelocidade()
plot2dangulos()
plot2dVelangulos()
