/*
 * RK.cpp
 *
 *  Created on: 29 de nov de 2019
 *      Author: roney
 */

#include "RK.h"

RK::RK(MatrixXf &A) {
	_dim = A.col(0).size();
	_K = MatrixXf::Zero(_dim, 4);
	_temp = MatrixXf::Zero(_dim, 1);

}
MatrixXf RK::ED(
		MatrixXf (*f)(const Ref<const MatrixXf>&, const Ref<const MatrixXf>&),
		float &dt, const MatrixXf &X, const MatrixXf &U) {
	_K.col(0) = dt * f(X, U);
	_K.col(1) = dt * f(X + _K.col(0) / 2.0f, U);
	_K.col(2) = dt * f(X + _K.col(1) / 2.0f, U);
	_K.col(3) = dt * f(X + _K.col(2), U);
	return X + (_K.col(0) + 2 * (_K.col(1) + _K.col(2)) + _K.col(3)) / 6;
}
