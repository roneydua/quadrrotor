#include "/home/roney/Dropbox/bibliotecasCplus/eigen/Eigen/Core"
#include "/home/roney/Dropbox/bibliotecasCplus/eigen/Eigen/Eigenvalues"

#include "calcMatrizes.h"
#include <Arduino.h>
#include <elapsedMillis.h>

elapsedMillis tempoExecucao; // variavel para medir tempo passado

using namespace Eigen;

const int8_t dim = 12;
MatrixXf A = MatrixXf::Zero(dim, dim);
MatrixXf B = MatrixXf::Zero(dim, 6);
const MatrixXf C = MatrixXf::Identity(dim, dim);
// matrizes Q, R e solucao de Ricattifloat number to a four bytes python
const MatrixXf Q = MatrixXf::Identity(dim, dim);
const MatrixXf R = 50.0f * MatrixXf::Identity(6, 6);
const MatrixXf W = C.transpose() * Q;
const MatrixXf V = W * C;
MatrixXf K(dim, dim);
MatrixXf E(dim, dim);
// instancia e inicializa a classe do integrador
int n = 1;                          // int(tf / dt);  // numero de iteracoes
MatrixXf X = MatrixXf::Zero(12, n); // vetor de estados
MatrixXf U = MatrixXf::Zero(6, n);
MatrixXf r = MatrixXf::Zero(12, n); // vetor de referencia
VectorXf data(6 + 1);               // vetor que concatena controle e tempo
MatrixXf DELTA(2 * dim, 2 * dim);
EigenSolver<MatrixXf> es(2 * dim);
MatrixXcf AVec = MatrixXcf::Zero(2 * dim, 2 * dim);
VectorXf AVal = VectorXf::Zero(2 * dim);

void setup() { Serial.begin(115200); }

struct esp32PC {
  float data[7];
};

void recebeDados(MatrixXf &rr, MatrixXf &XX) {
  // funcao le 96 byte compondo, atraves de uma union, um array de dimsao 48.
  // com a funcao Map convertemos o array padrao C++ em um vetor EIGEN
  union byte2float {
    float nfloat[12];
    uint8_t nbyte[sizeof(nfloat)];
  } b2f;
  for (size_t i = 0; i < 48; i++) {
    b2f.nbyte[i] = Serial.read();
  }
  rr = Map<MatrixXf>(b2f.nfloat, 12, 1);
  for (size_t i = 0; i < 48; i++) {
    b2f.nbyte[i] = Serial.read();
  }
  XX = Map<MatrixXf>(b2f.nfloat, 12, 1);
}
uint8_t bs[28];
float dataArray[7];

void enviaDados(MatrixXf &U, float tt) {
  data << U, 1.0e-3 * tt;
  Map<VectorXf>(dataArray, 7, 1) = data;
  memcpy(&bs, &dataArray, sizeof(bs));
  Serial.write(bs, 28);
}
// instacia da classe calcMatrizes
calcMatrizes calcMatrizes(A, B);

void loop() {

  if (Serial.available() == 96) {
    recebeDados(r, X);
    tempoExecucao = 0.0f;
    calcMatrizes.atualizaPlanta(X, A, B);
    E = B * R.inverse() * B.transpose();
    DELTA << A, -E, -V, -A.transpose();
    // EigenSover encontra os autovalores e autovetores das matriz _DELTA
    es.compute(DELTA);
    // loop responsavel por coletar os autovetores associados aos autovalores
    // estaveis
    AVec << es.eigenvectors();
    AVal << es.eigenvalues().real();
    for (int i = 0; i < 2 * dim; ++i) {
      for (int j = 0; j < 2 * dim; ++j) {
        if (AVal(j) > AVal(i)) {
          AVal.row(i).swap(AVal.row(j));
          AVec.col(i).swap(AVec.col(j));
        }
      }
    }
    K = (AVec.block(dim, 0, dim, dim) * AVec.block(0, 0, dim, dim).inverse())
            .real();
    U = R.inverse() * B.transpose() *
        (-K * X + (K * E - A.transpose()).inverse() * W * r);
        while (tempoExecucao<100) {

        }
    enviaDados(U, tempoExecucao);
  }
}
