/*
 * calcMatrizes.cpp
 *
 *  Created on: 25 de nov de 2019
 *      Author: roney
 */

#include "calcMatrizes.h"
calcMatrizes::calcMatrizes(MatrixXf &A, MatrixXf &B) {
	inicializa(A, B);
}

calcMatrizes::~calcMatrizes() {
}
// inicializa os valores constantes das matrizes A e B
void calcMatrizes::inicializa(MatrixXf &A, MatrixXf &B) {
	for (int var = 0; var < 12; var += 2) {
		A(var, var + 1) = 1.0;
	}
	B(1, 0) = 1.0f;
	B(3, 1) = 1.0f;
	B(5, 2) = 1.0f;
	_Ix = (_inercia(2) - _inercia(3)) / _inercia(1);
	_Iy = (_inercia(3) - _inercia(1)) / _inercia(2);
	_Iz = (_inercia(1) - _inercia(2)) / _inercia(3);
}
// funcao responsalvel para atualizar as matrizes de estados da planta
void calcMatrizes::atualizaPlanta(const VectorXf &X, MatrixXf &A, MatrixXf &B) {
	_sf = sinf(X(6));
	_cf = cosf(X(6));
	_st = sinf(X(8));
	_ct = cosf(X(8));
	_tt = tanf(X(8));
	_a(0) = _tt * (1.0f - _Iy * powf(_sf, 2) + _Iz * powf(_cf, 2));
	_a(1) = _st * _sf * _cf * (_Iy + _Iz);
	_a(2) = 1.0f / _ct + _ct * (powf(_cf, 2) - powf(_sf, 2)) * _Ix
			+ _st * _tt * (_Iy * powf(_sf, 2)) - _Iz * powf(_cf, 2);
	_a(3) = -_Ix * _sf * _cf;
	_a(4) = _sf * _cf * (_Ix * powf(_cf, 2) - (_Iy + _Iz) * powf(_st, 2));
	_a(5) = -_sf * _cf * (_Iz + _Iy);
	_a(6) = _ct * (-1.0f + _Iy * powf(_cf, 2) - _Iz * powf(_sf, 2));
	_a(7) = _st * _ct * (_Iz * powf(_sf, 2) - _Iy * powf(_cf, 2));
	_a(8) = 1.0f / _ct * (1.0f - _Iy * powf(_sf, 2) + _Iz * powf(_cf, 2));
	_a(9) = _tt * (1.0f + _Iy * powf(_sf, 2) - _Iz * powf(_cf, 2));

	A(7, 7) = _a(0) * X(9) + _a(1) * X(11);
	A(7, 9) = _a(2) * X(11) + _a(3) * X(9);
	A(7, 11) = _a(4) * X(11);
	A(9, 7) = _a(5) * X(9) + _a(6) * X(11);
	A(9, 9) = _a(1) * X(11);
	A(9, 11) = _a(7) * X(11);
	A(11, 7) = _a(8) * X(9) - _a(5) * X(11);
	A(11, 9) = _a(9) * X(11);
	A(11, 11) = -_a(1) * X(11);

	B(7, 3) = 1.0f / _inercia(1);
	B(7, 4) = _tt * _sf / _inercia(2);
	B(7, 5) = _tt * _cf / _inercia(3);
	B(9, 4) = _cf / _inercia(2);
	B(9, 5) = -_sf * _inercia(3);
	B(11, 4) = _sf / _ct / _inercia(2);
	B(11, 5) = _cf / _ct / _inercia(3);
}
MatrixXf calcMatrizes::atualizaRota(const VectorXf &r, float &dt,
		const Vector3f &U) {
	_r(0) = atanf(
			(U(0) * cosf(r(10)) + U(1) * sinf(r(10))) / (U(2) + 1 - _g(2)));
	_r(2) = asinf((U(0) * sinf(r(10)) - U(1) * cosf(r(10))) / (U + _g).norm());
	_r(1) = (_r(0) - r(0)) / dt;
	_r(3) = (_r(2) - r(2)) / dt;
	return _r;

}
