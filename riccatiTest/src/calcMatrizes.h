/*
 * calcMatrizes.h
 *
 *  Created on: 25 de nov de 2019
 *      Author: roney
 */
#ifndef CALCMATRIZES_H_
#define CALCMATRIZES_H_

#include "/home/roney/Dropbox/bibliotecasCplus/eigen/Eigen/Core"
#include <Arduino.h>

using namespace Eigen;
class calcMatrizes {
public:
	calcMatrizes(MatrixXf &A, MatrixXf &B);
	virtual ~calcMatrizes();
	void inicializa(MatrixXf &A, MatrixXf &B);
	void atualizaPlanta(const VectorXf &X, MatrixXf &A, MatrixXf &B);
	MatrixXf atualizaRota(const VectorXf &r, float &dt, const Vector3f &U);
private:
	// inercia do quadrirrotor [m Ix Iy Iz]
	Vector4f _inercia { 2.03f, 2*16.83e-3, 2*16.83e-3, 2*28.34e-3 };
	float _Ix, _Iy, _Iz;
	Vector3f _g { 0.0f, 0.0f, 9.82f }; // vetor gravidade
	float _braco = 0.26; // braco do motor
	// constantes dos motores
	float kf = 1.4351e-5;
	float km = 2.4086e-7;
	float _sf, _cf, _st, _ct, _tt;
	VectorXf _a = VectorXf::Zero(10);
	Vector4f _r = Vector4f::Zero();
};

#endif /* CALCMATRIZES_H_ */
