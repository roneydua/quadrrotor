/*
 * RK.h
 *
 *  Created on: 29 de nov de 2019
 *      Author: roney
 */

#ifndef RK_H_
#define RK_H_
#include "/home/roney/Dropbox/bibliotecasCplus/eigen/Eigen/Core"
#include <Arduino.h>

using namespace Eigen;

class RK {
public:
	RK(MatrixXf &A);
	int _dim;
	MatrixXf ED(
			MatrixXf (*f)(const Ref<const MatrixXf>&,
					const Ref<const MatrixXf>&), float &dt, const MatrixXf &X,
			const MatrixXf &U);
	MatrixXf _K;
	MatrixXf _temp;
};

/* namespace RK */

#endif /* RK_H_ */
