import numpy as np


def AREquaternion(A, V, E):
    """
    @brief      compute ARE

    @details    detailed description

    @param      matrix A, V and E

    @return     K solution of ARE
    """
    delta = np.vstack((np.hstack((A, -E)), np.hstack((-V, -A.T))))
    value, vectors = np.linalg.eig(delta)
    n = len(A)
    W = np.zeros((2*n, n), dtype="complex")
    k = 0
    for i in range(2*n):
        if value[i].real < 0.0:
            W[:, k] = vectors[:, i]
            k += 1
            if k == n:
                break
    # return delta

    return (W[n:, :] @ np.linalg.inv(W[:n, :])).real
