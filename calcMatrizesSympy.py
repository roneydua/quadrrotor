#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
caso com realimentacao dos estados de posicao e velocidade translacionais
"""
import numpy as np
import sympy as spy
from sympy import sin, cos, tan
import control

Ixx, Iyy, Izz = spy.symbols(r'I_{xx}, I_{yy}, I_{zz}', Real=True)  # [m Ixx Iyy Izz]
Ix, Iy, Iz, m, g = spy.symbols(r'I_{x}, I_{y}, I_{z}, m, g', Real=True)  # [Ix Iy Iz m g]
t = spy.symbols('t')
x = spy.Function(r'x')(t)
xp = spy.diff(x, t)
y = spy.Function(r'y')(t)
yp = spy.diff(y, t)
z = spy.Function(r'z')(t)
zp = spy.diff(z, t)

theta = spy.Function(r'\theta')(t)
thetap = spy.diff(theta, t)
fi = spy.Function(r'\phi')(t)
fip = spy.diff(fi, t)
psi = spy.Function(r'\psi')(t)
psip = spy.diff(psi, t)
X = spy.Matrix([x, xp, y, yp, z, zp, theta, thetap, fi, fip, psi, psip])
#braco = 0.450/2
X = spy.Matrix([x, xp, y, yp, z, zp, theta, 0.0, fi, 0.0, psi, 0.0])

#g = np.matrix([0,0,9.82]).T
#kf = 1.4351e-5
#km = 2.4086e-7
# constante proporcionais
#Kv = [10., 10., 10.]
#Ix = (Inercia[2]-Inercia[3])/Inercia[1]
#Iy = (Inercia[3]-Inercia[1])/Inercia[2]
#Iz = (Inercia[1]-Inercia[2])/Inercia[3]


def planta(X):
    sf = sin(X[6, 0])
    cf = cos(X[6, 0])
    st = sin(X[8, 0])
    ct = cos(X[8, 0])
    tt = tan(X[8, 0])
    cp = cos(X[10, 0])
    sp = sin(X[10, 0])
    Ap = spy.Matrix([[0., 1., 0., 0., 0., 0., 0, 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 1., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 1., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 1., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 1., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 1.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.]])
    a1 = tt*(1-Iy*sf**2+Iz*cf**2)
    a2 = st*sf*cf*(Iy+Iz)
    a3 = 1/ct+ct*(cf**2-cf**2)*Ix+st*tt*(Iy*sf**2-Iz*cf**2)
    a4 = -Ix*sf*cf
    a5 = sf*cf*(Ix*cf**2-(Iy+Iz)*st**2)
    a6 = -sf*cf*(Iz+Iy)
    a7 = ct*(-1+Iy*cf**2-Iz*sf**2)
    a8 = st*ct*(Iz*sf**2-Iy*cf**2)
    a9 = 1/ct*(1-Iy*sf**2+Iz*cf**2)
    a10 = tt*(1+Iy*sf**2-Iz*cf**2)


#    Coefientes de momento giroscopios nao programados
    Ap[7, 7] = a1*X[9, 0]+a2*X[11, 0]
    Ap[7, 9] = a3*X[11, 0] + a4*X[9, 0]
    Ap[7, 11] = a5*X[11, 0]
    Ap[9, 7] = a6*X[9, 0]+a7*X[11, 0]
    Ap[9, 9] = a2*X[11, 0]
    Ap[9, 11] = a8*X[11, 0]
    Ap[11, 7] = a9*X[9, 0]+a6*X[11, 0]
    Ap[11, 9] = a10*X[11, 0]
    Ap[11, 11] = -a2*X[11, 0]
    Bp = spy.Matrix(spy.zeros(12, 4))
    b1 = 1/Ixx
    b2 = tt*sf/Iyy
    b3 = tt*cf/Izz
    b4 = cf/Iyy
    b5 = -sf*Izz
    b6 = sf/ct/Iyy
    b7 = cf/ct/Izz
    Bp[1, 0] = (cp*st*cf+sp*sf)/m
    Bp[3, 0] = (sp*st*cf-cp*sf)/m
    Bp[5, 0] = (ct*cf)/m
    Bp[7, 1] = b1
    Bp[7, 2] = b2
    Bp[7, 3] = b3
    Bp[9, 2] = b4
    Bp[9, 3] = b5
    Bp[11, 2] = b6
    Bp[11, 3] = b7

    return Ap, Bp


def calcmatrizesCD():
    Cp = np.matrix(np.zeros((8, 12)))
    Cp[0, 0] = 1.0
    Cp[1, 1] = 1.0
    Cp[2, 2] = 1.0
    Cp[3, 3] = 1.0
    Cp[4, 4] = 1.0
    Cp[5, 5] = 1.0
    Cp[6, 10] = 1.0
    Cp[7, 11] = 1.0
    Dp = np.matrix(np.zeros((3, 4)))
    return Cp, Dp




def rotacao(X):
# =============================================================================
# Entrada: Vetores de estados, Saida: matrix de rotacao que leva os
#    vetores no sistema do copor x para o sistema inercial X
# =============================================================================
    return np.matrix([[cos(X[10,0])*cos(X[8,0]), cos(X[10,0])*sin(X[6,0])*sin(X[8,0])-cos(X[6,0])*sin(X[10,0]),
                sin(X[6,0])*sin(X[10,0])+cos(X[6,0])*cos(X[10,0])*sin(X[8,0]) , ],
               [cos(X[8,0])*sin(X[10,0]), cos(X[6,0])*cos(X[10,0])+sin(X[6,0])*sin(X[10,0])*sin(X[8,0]) ,cos(X[6,0])*sin(X[10,0])*sin(X[8,0])-cos(X[10,0])*sin(X[6,0])],
               [-sin(X[8,0]),cos(X[8,0])*sin(X[6,0]) ,cos(X[6,0])*cos(X[8,0])]])



def plantaSimplificada(X):
    sf = sin(X[6, 0])
    cf = cos(X[6, 0])
    st = sin(X[8, 0])
    ct = cos(X[8, 0])
    tt = tan(X[8, 0])
    cp = cos(X[10, 0])
    sp = sin(X[10, 0])
    Ap = spy.Matrix([[0., 1., 0., 0., 0., 0., 0, 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 1., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 1., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 1., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 1., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 1.],
                    [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.]])
#    a1 = spy.Function(r'a_{1}')(theta, fi, psi)
#    a2 = spy.Function(r'a_2')(theta, fi, psi)
#    a3 = spy.Function(r'a_3')(theta, fi, psi)
#    a4 = spy.Function(r'a_4')(theta, fi, psi)
#    a5 = spy.Function(r'a_5')(theta, fi, psi)
#    a6 = spy.Function(r'a_6')(theta, fi, psi)
#    a7 = spy.Function(r'a_7')(theta, fi, psi)
#    a8 = spy.Function(r'a_8')(theta, fi, psi)
#    a9 = spy.Function(r'a_9')(theta, fi, psi)
#    a10 = spy.Function(r'a_10')(theta, fi, psi)

    a1 = spy.symbols(r'a_{1}')
    a2 = spy.symbols(r'a_2')
    a3 = spy.symbols(r'a_3')
    a4 = spy.symbols(r'a_4')
    a5 = spy.symbols(r'a_5')
    a6 = spy.symbols(r'a_6')
    a7 = spy.symbols(r'a_7')
    a8 = spy.symbols(r'a_8')
    a9 = spy.symbols(r'a_9')
    a10 = spy.symbols(r'a_10')


#    Coefientes de momento giroscopios nao programados
    Ap[7, 7] = a1*X[9, 0]+a2*X[11, 0]
    Ap[7, 9] = a3*X[11, 0] + a4*X[9, 0]
    Ap[7, 11] = a5*X[11, 0]
    Ap[9, 7] = a6*X[9, 0]+a7*X[11, 0]
    Ap[9, 9] = a2*X[11, 0]
    Ap[9, 11] = a8*X[11, 0]
    Ap[11, 7] = a9*X[9, 0]+a6*X[11, 0]
    Ap[11, 9] = a10*X[11, 0]
    Ap[11, 11] = -a2*X[11, 0]
    Bp = spy.Matrix(spy.zeros(12, 4))
    b1 = spy.symbols(r'b_1', Real=True)
    b2 = spy.symbols(r'b_2')
    b3 = spy.symbols(r'b_3')
    b4 = spy.symbols(r'b_4')
    b5 = spy.symbols(r'b_5')
    b6 = spy.symbols(r'b_6')
    b7 = spy.symbols(r'b_7')
    Bp[1, 0] = spy.symbols(r'B_21')
    Bp[3, 0] = spy.symbols(r'B_41')
    Bp[5, 0] = spy.symbols(r'B_61')
    Bp[7, 1] = b1
    Bp[7, 2] = b2
    Bp[7, 3] = b3
    Bp[9, 2] = b4
    Bp[9, 3] = b5
    Bp[11, 2] = b6
    Bp[11, 3] = b7
    return Ap, Bp





Ap, Bp = plantaSimplificada(X)
Cp, Dp = calcmatrizesCD()
#ctrl = control.ctrb(Ap, Bp)
ctrol = control.obsv(Ap, Cp).T
