#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 09:36:05 2020
@author: roney
"""
import numpy as np


def screwMatrix(quat):
    q_x = np.zeros((3, 3))
    q_x[0, 1] = -quat[3]
    q_x[1, 0] = quat[3]
    q_x[0, 2] = quat[2]
    q_x[2, 0] = -quat[2]
    q_x[1, 2] = -quat[1]
    q_x[2, 1] = quat[1]
    return q_x


def Q(quat, right=False):
    # =========================================================================
    #   set right = True for compute right-quaternion matrix
    # =========================================================================
    S = np.zeros((4, 3))
    S[0, 0] = -quat[1]
    S[0, 1] = -quat[2]
    S[0, 2] = -quat[3]
    if right:
        S[1:, :] = quat[0]*np.identity(3) - screwMatrix(quat)
    else:
        S[1:, :] = quat[0]*np.identity(3) + screwMatrix(quat)
    return S

def S(quat, right=False):
    # =========================================================================
    #   set right = True for compute right-quaternion matrix
    # =========================================================================
    S = np.zeros((4, 4))
    if right:
        S[:, 1:] = Q(quat)
    else:
        S[:, 1:] = Q(quat, right=True)

    S[:, 0] = quat
    return S


def MultQuat(r, q, p):
    """
    @brief      Multiply two quaternion
    """
    r[0] = q[0] * p[0] - q[1]*p[1]-q[2]*p[2]-q[3]*p[3]
    r[1] = q[1] * p[0] + q[0]*p[1]-q[3]*p[2]+q[2]*p[3]
    r[2] = q[2] * p[0] + q[3]*p[1]+q[0]*p[2]-q[1]*p[3]
    r[3] = q[3] * p[0] - q[2]*p[1]+q[1]*p[2]+q[0]*p[3]
