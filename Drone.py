import sympy as sp
import numpy as np


class Drone:
    """Documentation for"""

    def __init__(self):
        self.Ix = 0.0451
        self.Iy = 0.0451
        self.Iz = 0.0170
        self.kf = 1.4351e-5
        self.km = 2.4086e-7
        self.b = 0.25
        self.grav = np.array([0.0, 0.0, 9.81])

        # moments of nertia of the body considering self.Ix = Iy kgm^2
        self.matA = np.array(np.zeros((9, 9)))
        self.matB = np.array(np.zeros((9, 6)))
        self.matB[:3, :3] = np.identity(3)
        self.matB[6, 3] = 1.0/self.Ix
        self.matB[7, 4] = 1.0/self.Ix
        self.matB[8, 5] = 1.0/self.Iz

    def update(self, r):
        rq = np.sqrt(1.0-r[3]**2-r[4]**2-r[5]**2) / 2.0
        # if np.isnan(rq):
        #     rq = 0.0
        self.matA[3, 6] = rq
        self.matA[3, 7] = -r[5]/2.0
        self.matA[3, 8] = r[4]/2.0

        self.matA[4, 6] = r[5]/2.0
        self.matA[4, 7] = rq
        self.matA[4, 8] = -r[3]/2.0

        self.matA[5, 6] = -r[4]/2.0
        self.matA[5, 7] = r[3]/2.0
        self.matA[5, 8] = rq

        # self.matA[6, 6] = 0.0
        self.matA[6, 7] = -self.Iy*r[8]/self.Ix
        self.matA[6, 8] = self.Iz*r[7]/self.Ix

        self.matA[7, 6] = self.Ix*r[8]/self.Iy
        # self.matA[7, 7] = 0.0
        self.matA[7, 8] = -self.Iz*r[6]/self.Iy

        self.matA[8, 6] = -self.Ix*r[7]/self.Iz
        self.matA[8, 7] = self.Iy*r[6]/self.Iz
        # self.matA[8, 8] = 0.0


    def Bm(self):
        return self.matB

    def Am(self):
        return self.matA
    def rotacoes(self, u):
        U1 = np.linalg.norm(u[:3]+self.grav)
        o = np.zeros((4))
        o[0] = U1/4.0/self.kf - 0.5*u[4]/self.kf/self.b-u[5]/4.0/self.km
        o[1] = U1/4.0/self.kf - 0.5*u[3]/self.kf/self.b+u[5]/4.0/self.km
        o[2] = U1/4.0/self.kf + 0.5*u[4]/self.kf/self.b-u[5]/4.0/self.km
        o[3] = U1/4.0/self.kf + 0.5*u[3]/self.kf/self.b+u[5]/4.0/self.km
        return np.sqrt(o)


class DorneSymbolic(object):
    """Documentation for DorneSymbolic

    """

    def __init__(self, states=False):

        self.Ix = sp.Symbol('I_x')
        self.Iz = sp.Symbol('I_z')
        # moments of nertia of the body considering self.Ix = Iy kgm^2
        r = sp.zeros(9, 1)
        self.matA = sp.Matrix(sp.zeros(9, 9))
        self.matB = sp.Matrix(sp.zeros(9, 6))
        self.matB[:3, :3] = sp.eye(3)
        self.matB[6, 3] = 1/self.Ix
        self.matB[7, 4] = 1/self.Ix
        self.matB[8, 5] = 1/self.Iz
        if states == True:
            for i in range(3):
                r[i] = sp.Symbol(r'V_'+str(i))
                r[i+3] = sp.Symbol(r'q_'+str(i+1))
                r[i+6] = sp.Symbol(r'\omega_'+str(i+1))
            rq = sp.sqrt(1-r[3]**2-r[4]**2-r[5]**2) / 2
        else:
            for i in range(9):
                r[i] = sp.Symbol(r'r_'+str(i+1))
            rq = sp.Symbol(r'r{\left(\mathbf{q}\right)}')
        self.matA[3, 6] = rq
        self.matA[3, 7] = -r[5]/2
        self.matA[3, 8] = r[4]/2
        self.matA[4, 6] = r[5]/2
        self.matA[4, 7] = rq
        self.matA[4, 8] = -r[3]/2
        self.matA[5, 6] = -r[4]/2
        self.matA[5, 7] = r[3]/2
        self.matA[5, 8] = rq
        # self.matA[6, 6] = 0.0
        self.matA[6, 7] = r[8]*self.Iz/self.Ix
        self.matA[6, 8] = -r[7]

        self.matA[7, 6] = -r[8]*self.Iz/self.Ix
        # self.matA[7, 7] = 0.0
        self.matA[7, 8] = r[6]

    def Bm(self):
        return self.matB

    def Am(self):
        return self.matA
