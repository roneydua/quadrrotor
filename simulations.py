#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 01:03:51 2020

@author: roney
"""
import sys
import numpy as np
from numpy import cos, sin, sqrt
from numpy.linalg import norm
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import Drone
import control as ctrl
from RiccatiSolver import*
from funcoesQuaternion import *
import control
# plt.style.use('roney.mplstyle')
quad = Drone.Drone()
coorSim = np.array([0, 1, 2])
DT = 0.01
t0 = 0.0
tf = 100.
MASS = 1.0
GRAV = np.array([0., 0., 9.81])  # m/s^2

stateName = ['$\dot{x}$', '$\dot{y}$', '$\dot{z}$',
             '$q_{1}$','$q_{2}$','$q_{3}$', '$\omega_{1}$', '$\omega_{2}$', '$\omega_{3}$']
t = np.arange(t0, tf, DT)
rc = np.zeros((4, t.size)) # control [Vx, Vy, Vz and psi]
# atitude inicial
rc[0, :4000] = 10.0#np.cos(np.pi*t/1.)
rc[1, 4000:] = 10.0#np.sin(np.pi*t/1.)
rc[2, :] = 0.
rc[3, :] = 0.0 * t
# control vector
u = np.zeros((6, t.size))
realU = np.zeros((4, t.size))
# orientation
q = np.zeros((4, t.size))
q[0, :] = 1.0
# dq = np.zeros((4, t.size))
# desire quaternion of attitude
qd = np.zeros((4, t.size))
qd[0, :] = 1.0
euler = np.zeros((3, t.size))


we = np.zeros((3, t.size))
x = np.zeros((9, t.size))
xd = np.zeros((9, t.size))
I3 = np.eye(3)



def quat2Euler(q):
    e = np.array([0., 0., 0.])
    e[0] = np.arctan2(q[0]*q[1]+q[2]*q[3], q[0]**2+q[3]**2-0.5)
    e[1] = -np.arcsin(2*q[1]*q[3]-2*q[0]*q[2])
    e[2] = np.arctan2(q[0]*q[3]+q[1]*q[2], q[0]**2+q[1]**2-0.5)
    return e


def conjugate(qq):
    qq[1:] *= -1.0
    return qq


def computeQuaternionDesire(qd, u_cont, rc):
    """Compute desire quaternion and rewrite qd value
    u_cont it is a 6x1 controle vector
    rc 4x1
    """
    Uv = u_cont[:3] + GRAV
    Tb = norm(Uv)
    # Uv = Uv.getA1()
    # rc = rcc.getA1()
    qd[0] = cos(0.5*rc[3])*sqrt(Tb + MASS*Uv[2])
    qd[1] = MASS*(Uv[0]*sin(1.5*rc[3])-Uv[1]*cos(1.5*rc[3])
                  ) / sqrt(Tb+MASS*Uv[2])
    qd[2] = MASS*(Uv[0]*cos(1.5*rc[3])+Uv[1]*sin(1.5*rc[3])
                  ) / sqrt(MASS*Uv[2]+Tb)
    qd[3] = -sin(0.5*rc[3])*sqrt(Tb + MASS*(Uv[2]))
    qd /= sqrt(2.0*Tb)


Rc = 100.*np.eye(6)
Rcinv = np.linalg.inv(Rc)
Qc = 500.0*np.eye(9)
E = quad.matB @ Rcinv @ quad.matB.T
W = 1.0 * Qc
V = 1.0 * Qc
K = np.zeros((9, 9))
s = np.zeros(9)
Q35 = Qc[3:6, 3:6]

dragCoeficienteArray = np.diag([.017, .017, 2*.001, 0., 0., 0., 0., 0., 0.])

def f(state):
    return (quad.matA-E@K)@state - E@s - dragCoeficienteArray@(np.abs(state)*state)

def RK(state):
    k1 = f(state)
    k2 = f(state+k1*DT/2.0)
    k3 = f(state+k2*DT/2.0)
    k4 = f(state+k3*DT)
    return state + DT/6.0 * (k1+2.0*(k2+k3)+k4)

def updateV_W(V, W, des, atual):
    W[3:6, 3:6] = atual[0]*(des[0]*I3 + screwMatrix(des))@Q35
    V[3:6, 3:6] = (des[0]*I3 + screwMatrix(des)) @ Q35 @ (des[0]*I3 - screwMatrix(des))
    V[3:6, 3:6] += I3 *( des[1:].T @ Q35@ (1.0/atual[0]*(screwMatrix(des)-des[0]*I3)@atual[1:]-des[1:]))

for i in range(1, t.size-1):
    # update states matrices
    quad.update(x[:, i])
    matrixControl = ctrl.ctrb(quad.matA, quad.matB)
    dimMatrixControl = np.linalg.matrix_rank(matrixControl)
    if dimMatrixControl != 9:
        print('problem controlability, dim='+ str(dimMatrixControl))
        print(x[:, i])
        sys.exit()
    # update desire orientation
    computeQuaternionDesire(qd[:, i], u[:, i-1], rc[:, i])
    updateV_W(V, W, qd[:, i], q[:, i])
    K = AREquaternion(quad.matA, V, E)
    # atualiza o vetor desejado
    xd[:3, i] = rc[:3, i]
    xd[3:6, i] = qd[1:, i]
    s = -np.linalg.inv(K @ E - quad.matA.T) @ W @ xd[:, i]
    u[:, i] = -Rcinv @ quad.matB.T @ (K @ x[:, i]+s)
    x[:, i+1] = RK(x[:, i])
    #update quaternion of attitude
    q[1:, i+1] = x[3:6, i+1]
    q[0, i+1] = sqrt(1.0-np.square(q[1:, i+1]).sum())
    realU[:, i+1] = quad.rotacoes(u[:, i])
    euler[:, i+1] = np.rad2deg(quat2Euler(q[:, i+1]))

def plot(indice=[]):
    fig, ax = plt.subplots(2, 1, num=1)
    for j in range(len(indice)):
        ax[0].plot(t, x[indice[j], :].T, label=stateName[indice[j]])
        ax[0].plot(t, rc[indice[j], :].T, label=r'$r_{'+stateName[indice[j]][1:-1]+'}$')
        ax[0].legend(ncol=len(indice))
    for i in range(9):
        if i not in indice:
            ax[1].plot(t, x[i, :].T, label=stateName[i])
    ax[1].legend(ncol=4)
    plt.tight_layout()

def plot3d():
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.plot(x[0,:].T, x[1,:].T, x[2,:].T)
    plt.show()




# plot3d()
def plotEuler():
    plt.figure(num='euler')
    plt.plot(t, euler.T)
    plt.legend([r'$\phi$',r'$\theta$',r'$\psi$'])
    plt.tight_layout()

def plotMotores():
    plt.figure(num='motores')
    plt.plot(t, realU.T)
    plt.legend([r'$M_1$',r'$M_2$',r'$M_3$', r'$M_4$'])
    plt.tight_layout()



def plotVelIner():
    plt.figure(num='VelocidadeTrans')
    plt.plot(t, x[:3, :].T)
    plt.plot(t, rc[:3, :].T, '--')
    plt.legend([r'$\dot{x}$',r'$\dot{y}$',r'$\dot{z}$', r'$r_\dot{x}$',r'$r_\dot{y}$',r'$r_\dot{z}$'])
    plt.tight_layout()


plotEuler()
# plotVelIner()
plot(coorSim)
plotMotores()
