#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep  2 15:02:42 2020

@author: roney
"""
import numpy as np
class RK(object):
    def __init__(self, x):
        n = len(x)
        k1 = np.zeros(n)
        k2 = np.zeros(n)